# miso-bmxtricks

A not so minimalistic example of Miso's isomorphic feature.

[Try online !](https://miso-bmxtricks.herokuapp.com)

![](miso-bmxtricks.mp4)

See also
[FPtje/miso-isomorphic-example](https://github.com/FPtje/miso-isomorphic-example).


## features

- web app in Haskell, using [Miso](https://github.com/dmjio/miso) (isomorphic
  SPA) and [Servant](https://www.servant.dev/) (type-safe routing)
- CSS using Bootstrap
- server API (JSON, static files)
- client routing
- XHR request
- database connection (SQLite)
- simple project configuration, using Cabal and Nix 
- build a docker image using Nix and deploy it on Heroku
- gitlab CI/CD...


## setup

- install Nix and Cachix

- activate binary cache:

```
cachix use miso-haskell
```


## develop the project

```
make database
make client
make server
make run
```

## build & run a release

```
nix-build nix/release.nix
cd result
./bin/server
```

## build & run a docker image

```
nix-build nix/docker.nix
docker load < result
docker run --rm -it -p 3000:3000 bmxtricks:latest
```

## deploy a docker image (manually)

```
heroku login
heroku container:login
heroku create miso-bmxtricks
docker tag bmxtricks:latest registry.heroku.com/miso-bmxtricks/web
docker push registry.heroku.com/miso-bmxtricks/web
heroku container:release web --app miso-bmxtricks
```

## deploy a docker image (using gitlab CI/CD)

- add a HEROKU_API_KEY variable in the Gitlab repo (see account setting in
  Heroku)
- create a container app in Heroku and add a APP_NAME variable in the Gitlab
  repo 
- uncomment the `production` job in `.gitlab-ci.yaml`
- push the master branch to update the app


