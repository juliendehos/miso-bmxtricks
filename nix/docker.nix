{ pkgs ? import <nixpkgs> {} }:

let

  bmxtricks = import ./release.nix;

  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@
  '';

in

  pkgs.dockerTools.buildImage {
    name = "bmxtricks";
    tag = "latest";
    created = "now";
    config = {
      WorkingDir = "${bmxtricks}";
      Entrypoint = [ entrypoint ];
      Cmd = [ "${bmxtricks}/bin/server" ];
    };
  }

