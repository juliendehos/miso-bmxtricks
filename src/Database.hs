{-# LANGUAGE OverloadedStrings          #-}

module Database where

import Common

import Database.SQLite.Simple

instance FromRow Bmxtrick where
    fromRow = Bmxtrick <$> field <*> field <*> field 

loadBmxtricks :: String -> IO [Bmxtrick]
loadBmxtricks dbfile = 
    withConnection dbfile (`query_` "SELECT name, video, image FROM bmxtricks")

