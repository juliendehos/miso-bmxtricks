{-# LANGUAGE OverloadedStrings          #-}

import Common
import View

import Data.Aeson (decodeStrict)
import Data.Maybe (fromMaybe)
import JavaScript.Web.XMLHttpRequest
import Miso
import Miso.String (ms)

main :: IO ()
main = miso $ \ currentUri -> App 
    { initialAction = FetchBmxtricks
    , model = createModel currentUri
    , update = updateModel
    , view = viewModel
    , events = defaultEvents
    , subs = [ uriSub SetUri ]
    , mountPoint = Nothing
    , logLevel = Off
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel (SetBmxtricks bmxtricks) m = noEff m { modelBmxtricks = bmxtricks }
updateModel FetchBmxtricks m = m <# (SetBmxtricks <$> xhrBmxtricks)
updateModel (SelectBmxtrick bmxtrick) m = 
    m { modelSelected = Just bmxtrick } <# pure (ChangeUri videoRoute)
updateModel (SetUri uri) m = noEff m { modelUri = uri }
updateModel (ChangeUri uri) m = m <# (pushURI uri >> pure NoOp)

xhrBmxtricks :: IO [Bmxtrick]
xhrBmxtricks = 
    fromMaybe [] . decodeStrict . fromMaybe "" . contents <$> xhrByteString req
    where req = Request GET uri Nothing [] False NoData
          uri = ms $ show linkBmxtricks

