{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module Common where

import           Data.Aeson (FromJSON, ToJSON)
import           Data.Proxy (Proxy(..))
import qualified Data.Text as T
import           GHC.Generics (Generic)
import           Miso
import           Network.URI (URI)
import           Servant.API
import           Servant.Links


-- model

data Bmxtrick = Bmxtrick
    { bmxtrickName :: T.Text
    , bmxtrickVideo :: T.Text
    , bmxtrickImage :: T.Text
    } deriving (Eq, Generic)

instance FromJSON Bmxtrick
instance ToJSON Bmxtrick

data Model = Model 
    { modelBmxtricks :: [Bmxtrick]
    , modelSelected :: Maybe Bmxtrick
    , modelUri :: URI
    } deriving (Eq)

createModel :: URI -> Model
createModel = Model [] Nothing


-- actions

data Action
    = NoOp
    | SetBmxtricks [Bmxtrick]
    | FetchBmxtricks
    | SelectBmxtrick Bmxtrick
    | SetUri URI
    | ChangeUri URI
    deriving (Eq)


-- client routes

type HomeRoute = View Action
type VideoRoute = "video" :> View Action
type ClientRoutes = HomeRoute :<|> VideoRoute

homeRoute, videoRoute :: URI
homeRoute = linkURI $ safeLink (Proxy @ClientRoutes) (Proxy @HomeRoute)
videoRoute = linkURI $ safeLink (Proxy @ClientRoutes) (Proxy @VideoRoute)


-- common client/server routes 

type BmxtricksApi = "bmxtricks" :>  Get '[JSON] [Bmxtrick]
type StaticApi = Raw 

type PublicApi = BmxtricksApi :<|> StaticApi 

linkBmxtricks, linkStatic :: URI
linkBmxtricks = linkURI $ safeLink (Proxy @PublicApi) (Proxy @BmxtricksApi)
linkStatic = linkURI $ safeLink (Proxy @PublicApi) (Proxy @StaticApi)

mkStatic :: T.Text -> T.Text
mkStatic filename = T.concat [T.pack $ show linkStatic, "/", filename]

