{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module View where

import Common

import qualified Data.Map as M
import           Data.Proxy (Proxy(..))
import           Miso
import           Miso.String (ms)
import           Servant.API


clientViews :: (Model -> View Action) :<|> (Model -> View Action)
clientViews = homeView :<|> videoView

viewModel :: Model -> View Action
viewModel m = 
    case runRoute (Proxy @ClientRoutes) clientViews modelUri m of
        Left _ -> text "not found"
        Right v -> v

homeView :: Model -> View Action
homeView m = div_ [class_ "container"]
    [ navDom
    , homeDom
    , tricksDom m
    , aboutDom
    ]

navDom :: View Action
navDom = nav_ [class_ "navbar navbar-expand"]
    [ a_ [class_ "nav-item nav-link", href_ "/#bmx-tricks"] [text "Bmx Tricks"]
    , a_ [class_ "nav-item nav-link", href_ "/#the-tricks"] [text "The tricks"]
    , a_ [class_ "nav-item nav-link", href_ "/#about"] [text "About"]
    ]

mkContent :: [View action] -> View action
mkContent content = div_ [class_ "row"] 
    [div_ [class_ "container"] 
        ( content ++
            [ p_ [style_ $ M.singleton "text-align" "right" ]
                 [a_ [href_ "#"] [text "Top"]]
            , hr_ []
            ]
        )
    ]

homeDom :: View Action
homeDom = mkContent 
    [ h1_ [id_ "bmx-tricks"] [text "Bmx Tricks"]
    , p_ [] 
        [ text "Learn some bmx tricks, thanks to "
        , a_ [href_ "https://www.youtube.com/channel/UCaWcjg3UyzT0cHAX1D693GA"]
             [text "Greg Masson"]
        , text " !"
        ]
    ]

tricksDom :: Model -> View Action
tricksDom m = mkContent 
    [ h1_ [id_ "the-tricks"] [text "The tricks"]
    , div_ [style_ (M.fromList [ ("display", "flex")
                               , ("flex-wrap", "wrap")
                               , ("align-items", "flex-end") ])]
           (map formatBmxtrick (modelBmxtricks m))
    ]

formatBmxtrick :: Bmxtrick -> View Action
formatBmxtrick bt = 
    div_ [ onClick (SelectBmxtrick bt)
         , style_ (M.fromList [("margin", "20px"), ("text-align", "center")]) 
         ]
         [ img_ [ class_ "img-fluid"
                , src_ (ms $ mkStatic $ bmxtrickImage bt)
                ]
         , figcaption_ [] [ text (ms $ bmxtrickName bt) ]
         ]

aboutDom :: View Action
aboutDom = mkContent 
    [ h1_ [id_ "about"] [text "About"]
    , p_ [] 
        [ text "This web site is implemented in "
        , a_ [href_ "https://haskell.org/"] [text "Haskell"]
        , text ", using "
        , a_ [href_ "https://haskell-miso.org/"] [text "Miso"]
        , text " and "
        , a_ [href_ "https://www.servant.dev/"] [text "Servant"]
        , text ". "
        ]
    , p_ [] [ a_ [href_ "https://gitlab.com/juliendehos/miso-bmxtricks"]
                 [text "source code"] ]
    ]

videoView :: Model -> View Action
videoView m = div_ [class_ "container"]
    [ div_ [class_ "container"]
        [ videoDom m
        , button_ [class_ "btn btn-primary", onClick (ChangeUri homeRoute)]
                  [text "Back"]
        ]
    ]

videoDom :: Model -> View Action
videoDom m = case modelSelected m of
    Nothing -> p_ [] [text "no trick selected"]
    Just bmxtrick -> div_ [class_ "container"]
        [ h1_ [] [text (ms $ bmxtrickName bmxtrick)]
        , video_
            [preload_ "metadata", controls_ True, class_ "img-fluid"] 
            [source_ [src_ (ms $ mkStatic $ bmxtrickVideo bmxtrick), type_ "video/mp4"]]
        ]

