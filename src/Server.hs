{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

import           Common
import           Database
import           View

import           Control.Monad.IO.Class (liftIO)
import           Data.Maybe (fromMaybe)
import           Data.Proxy (Proxy(..))
import qualified Lucid as L
import           Miso
import           Network.Wai.Handler.Warp (run)
import           Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import           Network.Wai.Middleware.RequestLogger (logStdoutDev)
import           Servant
import           System.Environment (lookupEnv)


-- main program and data
 
main :: IO ()
main = do
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    dbfile <- fromMaybe "data/bmxtricks.db" <$> lookupEnv "DBFILE"
    putStrLn $ "listening port " ++ show port ++ "..."
    run port 
        $ logStdoutDev 
        $ gzip def { gzipFiles = GzipCompress }
        $ serve (Proxy @ServerApi) (server dbfile)


-- server api and app

type ServerApi
    =    BmxtricksApi
    :<|> ToServerRoutes ClientRoutes HtmlPage Action
    :<|> StaticApi

server :: String -> Server ServerApi
server dbfile
    =    liftIO (loadBmxtricks dbfile)
    :<|> (handleHome :<|> handleVideo)
    :<|> serveDirectoryWebApp "static"

handleHome, handleVideo :: Handler (HtmlPage (View Action))
handleHome = pure $ HtmlPage $ homeView $ createModel homeRoute
handleVideo = pure $ HtmlPage $ videoView $ createModel videoRoute


-- view rendering

newtype HtmlPage a = HtmlPage a
    deriving (Show, Eq)

instance L.ToHtml a => L.ToHtml (HtmlPage a) where
    toHtmlRaw = L.toHtml
    toHtml (HtmlPage x) = L.doctypehtml_ $ do
        L.head_ $ do
            L.meta_ [L.charset_ "utf-8"]
            L.meta_ [L.name_ "viewport", L.content_ "width=device-width, initial-scale=1, shrink-to-fit=no"]
            L.link_ [L.rel_ "stylesheet", L.href_ (mkStatic "bootstrap.min.css")]
            L.with 
                (L.script_ mempty) 
                [L.src_ (mkStatic "all.js"), L.async_ mempty, L.defer_ mempty] 
        L.body_ $ L.toHtml x

