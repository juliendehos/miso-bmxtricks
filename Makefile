client:
	nix-shell -A client --run "cabal --builddir=dist-client --config-file=config/client.config build client"
	mkdir -p static
	ln -sf ../`find dist-client/ -name all.js` static/

server:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config build server"

database:
	rm -f data/bmxtricks.db
	sqlite3 data/bmxtricks.db < data/bmxtricks.sql

run:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config run server"

docker-build:
	nix-build nix/docker.nix
	docker load < result

docker-run:
	docker run --rm -it -p 3000:3000 bmxtricks:latest

clean:
	rm -rf dist-* static/all.js data/bmxtricks.db

