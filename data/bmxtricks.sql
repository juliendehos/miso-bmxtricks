-- sqlite3 bmxtricks.db < bmxtricks.sql

CREATE TABLE bmxtricks (
  id INTEGER PRIMARY KEY, 
  name TEXT,
  video TEXT,
  image TEXT
);

INSERT INTO bmxtricks VALUES(0, '360 over the spine', 'bmx-360-spine.mp4', 'bmx-360-spine.png');
INSERT INTO bmxtricks VALUES(1, '360 tailwhip', 'bmx-360-tailwhip.mp4', 'bmx-360-tailwhip.png');
INSERT INTO bmxtricks VALUES(2, 'Alley-oop', 'bmx-alley-oop.mp4', 'bmx-alley-oop.png');
INSERT INTO bmxtricks VALUES(3, 'Flair', 'bmx-flair.mp4', 'bmx-flair.png');
INSERT INTO bmxtricks VALUES(4, 'Lawnmower', 'bmx-lawnmower.mp4', 'bmx-lawnmower.png');
INSERT INTO bmxtricks VALUES(5, 'Tuck no-hander', 'bmx-tuck-nohander.mp4', 'bmx-tuck-nohander.png');

